﻿
namespace Repository.ElasticSearch
{
	using System;
	using Nest;
	using Repository.ElasticSearch.Model;

	public class ElasticSearchClient : IElasticSearchClient
	{
		

		public ElasticSearchClient(IElasticClient client,string indexName)
		{
			this.Client = client;
			var result = this.Client.IndexExists(indexName);


			if (!result.Exists)
			{
				this.Client
				    .CreateIndex(indexName,r => r.Settings(s => s.NumberOfShards(1))
				                 .Mappings( x=> x.Map<Product>(product => product.AutoMap())));
			}
		}

		public IElasticClient Client
		{
			get;
		}   
	}
}
