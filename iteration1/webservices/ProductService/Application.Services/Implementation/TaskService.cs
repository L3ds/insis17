﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Crosscutting;
using Application.DTO;
using Repository;
using ElasticModel = Repository.ElasticSearch.Model;

namespace Application.Services
{
    public class TaskService : ITaskService
    {

        private readonly IElasticRepository<ElasticModel.Task> taskRepository;


        public TaskService(IElasticRepository<ElasticModel.Task> taskrepository)
		{
			Guard.GuardArgumentNull(taskrepository, "taskrepository");
			this.taskRepository = taskrepository;
		}

		public Task GetTaskById(int id)
		{
			var result = this.taskRepository.FindById(id);

			if (result == null)
			{
				return null;
			}

            return this.MapTask(result);
		}

		public IEnumerable<Task> GetAllTasks()
		{
			var result =  this.taskRepository.GetAll();

            return result.Select(x => this.MapTask(x));
		}

        public bool InsertTask(Task task)
		{
			var elasticDocument = new ElasticModel.Task
			{
				Description = task.Description,
                Time = TimeSpan.FromMinutes(task.Time),
				Name = task.Name
			};


			return this.taskRepository.Insert(elasticDocument);
		}

        public IEnumerable<Task> GetTasks(IEnumerable<int> tasksId)
        {
            var listOfTasks = new List<Task>();

            foreach (var taskId in tasksId)
            {
              var task = this.taskRepository.FindById(taskId);
                listOfTasks.Add(this.MapTask(task));
            }

            return listOfTasks;
        }


        private Task MapTask(ElasticModel.Task task)
        {
            return new Task
            {
                Description = task.Description,
                Id = task.Id,
                Name = task.Name,
                Time = (int)task.Time.TotalMinutes
            };
        }
    }
}
