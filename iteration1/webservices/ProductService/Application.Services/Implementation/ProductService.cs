﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Crosscutting;
using Application.DTO;
using Repository;
using ElasticModel = Repository.ElasticSearch.Model;

namespace Application.Services
{
	public class ProductService : IProductService
    {

		private readonly IElasticRepository<ElasticModel.Product> productRepository;


		public ProductService(IElasticRepository<ElasticModel.Product> productRepository)
		{
			Guard.GuardArgumentNull(productRepository, "productRepository");
			this.productRepository = productRepository;
		}

		public Product GetProductById(int id)
		{
			var result = this.productRepository.FindById(id);

			if (result == null)
			{
				return null;
			}

			return new Product
			{
				Description = result.Description,
				Id = result.Id,
				Name = result.Name,
				Price = result.Price,
				TaskIds = result.Tasks?.Select(x => x.Id)
			};
		}

		public IEnumerable<Product> GetProducts()
		{
			var result =  this.productRepository.GetAll();

			return result.Select(
				x => new Product { 
				Description = x.Description,
				Id = x.Id,
				Name = x.Name,
				Price = x.Price,
				TaskIds = x.Tasks.Select(t => t.Id)
			});
		}

		public bool InsertProduct(Product product)
		{
			var elasticDocument = new ElasticModel.Product
			{
				Description = product.Description,
				Price = product.Price,
				Name = product.Name,
				Tasks = new List<ElasticModel.Task>()
			};

			if (product.TaskIds != null && product.TaskIds.Any())
			{
				elasticDocument.Tasks = product
					.TaskIds
					.Select(t => new ElasticModel.Task
					{
						Id = t
					});
			}

			return this.productRepository.Insert(elasticDocument);
		}

        public bool UpdateProduct(Product product)
        {
            var elasticDocument = new ElasticModel.Product
            {
                Id=product.Id,
                Description = product.Description,
                Price = product.Price,
                Name = product.Name,
                Tasks = new List<ElasticModel.Task>()
            };

            if (product.TaskIds != null && product.TaskIds.Any())
            {
                elasticDocument.Tasks = product
                    .TaskIds
                    .Select(t => new ElasticModel.Task
                    {
                        Id = t
                    });
            }
                       return this.productRepository.Update(elasticDocument);
        }
    }
}
