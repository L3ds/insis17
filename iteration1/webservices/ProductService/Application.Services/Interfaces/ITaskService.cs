﻿using System;
using System.Collections.Generic;
using Application.DTO;

namespace Application.Services
{
	public interface IProductService
	{
		IEnumerable<Product> GetProducts();

		Product GetProductById(int id);

		bool InsertProduct(Product product);

        bool UpdateProduct(Product product);

	}
}
