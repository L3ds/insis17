﻿using System;
namespace Repository.ElasticSearch.Model
{
	public class Task
	{

		public int Id
		{
			get;
			set;
		}

		public string Description
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public TimeSpan Time
		{
			get;
			set;
		}
	}
}
