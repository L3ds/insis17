﻿namespace Application.Crosscutting.Bootstrapper
{
	using System.IO;
	using Services;
	using Nest;
	using Newtonsoft.Json;
	using Repository;
	using Repository.ElasticSearch;
	using SimpleInjector;
	using System;
	using Repository.ElasticSearch.Model;

	public static class Bootstrapper
	{
		private static ApplicationSettings settings;

		public static void LoadDependencies(Container container, ApplicationSettings Appsettings)
		{
			settings = Appsettings;
			RegisterServices(container);
			RegisterRepositories(container);
			RegisterClients(container);

		}

		private static void RegisterServices(Container container)
		{
			container.Register<IProductService, ProductService>();
            container.Register<ITaskService, TaskService>();
		}

		private static void RegisterRepositories(Container container)
		{
			container.Register<IElasticRepository<Product>>(
				() => new ProductRepository(container.GetInstance<IElasticSearchClient>(), settings.ElasticSearch.IndexName));
			container.Register<IElasticRepository<Task>>(
				() => new TaskRepository(container.GetInstance<IElasticSearchClient>(), settings.ElasticSearch.IndexName));
		}

		private static void RegisterClients(Container container)
		{
			var node = new Uri(settings.ElasticSearch.Endpoint);
			var connectionSettings = new ConnectionSettings(
				node
			);

			container.Register<IElasticClient>(() => new ElasticClient(connectionSettings));
			container.Register<IElasticSearchClient>(() =>
					 new ElasticSearchClient(container.GetInstance<IElasticClient>(),
											settings.ElasticSearch.IndexName));


		}



		private static void LoadConfiguration()
		{
			using (var sr = new StreamReader(File.OpenRead("appsettings.json")))
			using (var jsonTextReader = new JsonTextReader(sr))
			{
				JsonSerializer serializer = new JsonSerializer();
				settings = serializer.Deserialize<ApplicationSettings>(jsonTextReader);

			}

		}

	}
}
