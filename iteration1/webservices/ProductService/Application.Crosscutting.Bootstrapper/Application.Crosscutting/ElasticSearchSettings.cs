﻿using System;
namespace Application.Crosscutting
{
	public class ElasticSearchConfiguration
	{
		public ElasticSearchConfiguration()
		{
		}

		public string Endpoint {
			get;
			set;
		}

		public string IndexName
		{
			get;
			set;
		}
	}
}
