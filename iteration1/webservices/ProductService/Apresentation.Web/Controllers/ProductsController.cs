﻿using System;
using System.Collections.Generic;
using Application.DTO;
using Application.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Apresentation.Web
{
    [Route("api/[controller]")]
    public class ProductsController : Controller
    {
        private readonly IProductService service;
        private readonly ITaskService taskService;

        public ProductsController(IProductService service,ITaskService taskService)
        {
            this.service = service;
            this.taskService = taskService;
        }

    
        [HttpGet]
        public IEnumerable<Product> Get()
        {
            return this.service.GetProducts();
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result =  this.service.GetProductById(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet("{id}/tasks")]
        public IActionResult GetProductTasks(int id)
        {
            var result = this.service.GetProductById(id);

            if (result == null)
            {
                return NotFound("Product doesn't exists");
            }

            var tasks = this.taskService.GetTasks(result.TaskIds);

            return Ok(tasks);
        }

        [HttpPost("{id}/tasks")]
        public IActionResult TaskPost(int id,[FromBody] int[] value)
        {
            if(value==null || value.Length==0 )
            {
                return BadRequest("No Tasks sent");
            }

            var result = this.service.GetProductById(id);

            if (result == null )
            {
                return NotFound("Product doesn't exists");
            }

            var tasks = this.taskService.GetTasks(value);

            if(tasks.Count() != value.Count())
            {
                if(tasks==null)
                {
                    return BadRequest("Tasks are not created");
                }

                var missingValues = value.Where(x => !tasks.Any(t => t.Id==x ));

                return BadRequest($"Tasks {missingValues.Select(x => x.ToString())} are not created");
            }

            result.TaskIds = result.TaskIds.Concat(value).ToList();

            this.service.UpdateProduct(result);

            return Ok();
        }

        [HttpPost]
        public IActionResult Post([FromBody]Product newProduct)
        {


            if (newProduct!= null && this.service.InsertProduct(newProduct))
            {
                return Ok();
            }

            return BadRequest();
        }

        
    }
}

