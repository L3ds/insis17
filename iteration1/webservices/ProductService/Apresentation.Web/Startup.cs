﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Crosscutting;
using Application.Crosscutting.Bootstrapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SimpleInjector;
using SimpleInjector.Integration.AspNetCore;
using SimpleInjector.Integration.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Swagger;

namespace Apresentation.Web
{
    public class Startup
    {
		private Container container = new Container();

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();


        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();

			services.AddSingleton<IControllerActivator>(
		  new SimpleInjectorControllerActivator(container));
			services.AddSingleton<IViewComponentActivator>(
				new SimpleInjectorViewComponentActivator(container));

            // Add our repository type
          

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Product Service", Version = "v1" });
            });

        }

		// Configure is called after ConfigureServices is called.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env,
			ILoggerFactory factory)
		{
			app.UseSimpleInjectorAspNetRequestScoping(container);


			container.Options.DefaultScopedLifestyle = new AspNetRequestLifestyle();

			InitializeContainer(app);

			container.Verify();

			factory.AddConsole(Configuration.GetSection("Logging"));
			factory.AddDebug();

			app.UseMvc();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Product Service V1");
            });
		}


		private void InitializeContainer(IApplicationBuilder app)
		{
			container.RegisterMvcControllers(app);
			container.RegisterMvcViewComponents(app);

			Bootstrapper.LoadDependencies(container, Configuration.Get<ApplicationSettings>());
		}
    }
}
