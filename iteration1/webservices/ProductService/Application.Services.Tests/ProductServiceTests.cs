using System;
using Moq;
using NUnit;
using NUnit.Framework;
using Repository;
using ElasticModel = Repository.ElasticSearch.Model;

namespace Application.Services.Tests
{
    public class ProductServiceTests
    {
		
		private ProductService service;


		private Mock<IElasticRepository<ElasticModel.Product>> productRepositoryMock;

		[SetUp]
		public void Setup()
		{
			this.productRepositoryMock = new Mock<IElasticRepository<ElasticModel.Product>>();
			this.service = new ProductService(this.productRepositoryMock.Object);
		}


		[Test]
        public void ProductService_ConstructorWithNullRepository_ThrowsNullException()
        {
			Assert.Throws<ArgumentNullException>( () => new ProductService(null));
        }
    }
}
