﻿
namespace Application.DTO
{
    public class Task
	{
		public int Id
		{
			get;
			set;
		}	

		public int Time 
		{
			get;
			set;
		}

        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }
	}
}
