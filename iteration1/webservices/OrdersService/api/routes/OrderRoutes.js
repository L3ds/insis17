'use strict';

module.exports = (router) => {
	const orders = require('../controllers/OrderController');

	router.route('/orders')
		.post(orders.createOrder)
		.get(orders.listOrders);

	router.route('/orders/:orderId')
		.get(orders.readOrder)
		.put(orders.updateOrder)
		.delete(orders.removeOrder);
};
