'use strict';

module.exports = (router) => {
	const subscriptions = require('../controllers/SubscriptionController');

	router.route('/subscriptions')
		.post(subscriptions.createSubscription)
		.get(subscriptions.listSubscriptions);
};
