'use strict';
const _ = require('lodash');
const Order = require('../models/OrderModel');

exports.createOrder = function(req, res) {
  const order = new Order(req.body);

  order.save(function(err, order) {

    if (err) {
      res.send(err);
    }

    res.json({ createdOrder: order });
  });
};

exports.listOrders = function(req, res) {
  Order.find(function(err, order) {
    if (err) {
      res.send(err);
    }

    res.json(order);
  });
};

exports.readOrder = function(req, res) {
  Order.findById(req.params.orderId, function(err, order) {
    if (err) {
      res.send(err);
    }
    res.json({ order: order });
  });
};


exports.updateOrder = function(req, res) {
  Order.findById(req.params.orderId, function(err, order) {
    if (err) {
      res.send(err);
    }

    _.extend(order, req.body);

    order.save(function(err) {
      if (err) {
        res.send(err);
      }

      res.json({ message: 'Order updated', updatedOrder: order });
    })
  });
};


exports.removeOrder = function(req, res) {
  Order.remove({ _id: req.params.orderId }, function(err, order) {
    if (err) {
      res.send(err);
    }
    res.json({ message: 'Order deleted' });
  });
};
