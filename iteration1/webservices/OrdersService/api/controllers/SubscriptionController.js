'use strict';
const Subscription = require('../models/SubscriptionModel');

exports.createSubscription = function(req, res) {
  console.log(req.body);
  const subscription = new Subscription(req.body);

  subscription.save(function(err, subscription) {
    if (err) {
      res.send(err);
    }

    res.json({ createdSubscription: subscription });
  });
};

exports.listSubscriptions = function(req, res) {
  Subscription.find(function(err, subscription) {
    if (err) {
      res.send(err);
    }

    res.json(subscription);
  });
};
