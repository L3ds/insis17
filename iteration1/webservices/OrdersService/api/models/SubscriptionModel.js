// api/models/OrderModel.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SubscriptionSchema = new Schema({
	source: {
		type: String,
		required: true
	},

	key: {
		type: String,
		required: true
	},

	ordersURL: {
		type: String,
		required: true
	},

    proposalsURL: {
		type: String,
		required: true
	}
});

const mongooseModel = mongoose.model('Subscriptions', SubscriptionSchema);
mongooseModel.remove({ justOne: false });
console.log('Reset Subscriptions');

module.exports = mongooseModel;
