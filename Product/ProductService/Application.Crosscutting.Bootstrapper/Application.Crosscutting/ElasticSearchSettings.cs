﻿using System;
namespace Application.Crosscutting
{
	public class ElasticSearchConfiguration
	{
		public ElasticSearchConfiguration()
		{
		}

		public string Endpoint {
			get;
			set;
		}

		public string IndexName
		{
			get;
			set;
		}

        public bool Authentication
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
       
        }

        public string Password
        {
            get;
            set;
        }
    }
        
}
