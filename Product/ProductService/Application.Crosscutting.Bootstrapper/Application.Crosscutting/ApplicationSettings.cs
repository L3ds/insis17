﻿using System;

namespace Application.Crosscutting
{
    public class ApplicationSettings
    {


		public ElasticSearchConfiguration ElasticSearch
		{
			get;
			set;
		}

		public LoggingSettings Logging
		{
			get;
			set;
		}
	}
}
