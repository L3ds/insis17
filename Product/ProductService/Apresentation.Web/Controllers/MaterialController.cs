﻿using Application.DTO;
namespace Apresentation.Web
{
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Mvc;
    using Application.Services;

    [Route("api/[controller]")]
    public class MaterialController : Controller
    {

        private readonly IMaterialService service;

        public MaterialController(IMaterialService service)
        {
            this.service = service;
        }

        [HttpGet]
        public IEnumerable<Material> Get()
        {
            return this.service.GetAll();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Material Get(int id)
        {
            var material = this.service.GetById(id);

            if(material==null)
            {
                BadRequest("Material not found");
            }

            return material;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]Material value)
        {
           var result =  this.service.Insert(value);

            if (!result)
                BadRequest();

            Ok();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Material value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
