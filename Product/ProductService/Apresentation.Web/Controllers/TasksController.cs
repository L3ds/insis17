﻿using System;
using System.Collections.Generic;
using Application.DTO;
using Application.Services;
using Microsoft.AspNetCore.Mvc;

namespace Apresentation.Web
{
	[Route("api/[controller]")]
	public class TaskController : Controller
	{
		private readonly ITaskService service;

		public TaskController(ITaskService service)
		{
			this.service = service;
		}

		// GET api/values
		[HttpGet]
		public IEnumerable<Task> Get()
		{
            return this.service.GetAllTasks();
		}

		[HttpGet("{id}")]
		public IActionResult Get(int id)
		{
			var result =  this.service.GetTaskById(id);

			if (result == null)
			{
				return NotFound();
			}

			return Ok(result);
		}

		[HttpPost]
        public IActionResult Post([FromBody]Task task)
		{
            if (task == null)
            {
                return BadRequest();
            }
            
			if (this.service.InsertTask(task))
			{
				return Ok();
			}

			return BadRequest();
		}

		// PUT api/values/5
		[HttpPut("{id}")]
		public void Put(int id, [FromBody]string value)
		{
		}

		// DELETE api/values/5
		[HttpDelete("{id}")]
		public void Delete(int id)
		{
		}
	}
}

