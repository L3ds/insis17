﻿using System;
using System.Collections.Generic;
using Application.DTO;
using Application.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Apresentation.Web
{
    [Route("api/[controller]")]
    public class ProductsController : Controller
    {
        private readonly IProductService service;
        private readonly ITaskService taskService;
        private readonly IMaterialService materialService;

        public ProductsController(
            IProductService service,
            ITaskService taskService,
            IMaterialService materialService)
        {
            this.service = service;
            this.taskService = taskService;
            this.materialService = materialService;
        }

    
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(this.service.GetProducts());
        }

		[HttpGet("abrv")]
		public IActionResult GetAbreviated()
		{
			var productsList = this.service.GetProducts();
            var abreviatedProducts = new List<Object>();

            foreach (var item in productsList)
            {
                abreviatedProducts.Add(new { 
                    Id = item.Id,
                    Name = item.Name,
                    Description = item.Description
                });
            }

            return Ok(new { products = abreviatedProducts });
		}

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result =  this.service.GetProductById(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet("{id}/tasks")]
        public IActionResult GetProductTasks(int id)
        {
            var result = this.service.GetProductById(id);

            if (result == null)
            {
                return NotFound("Product doesn't exists");
            }

            var tasks = this.taskService.GetTasks(result.TaskIds);

            return Ok(new { tasks = tasks });
        }

        [HttpPost("{id}/tasks")]
        public IActionResult TaskPost(int id,[FromBody] int[] value)
        {
            if(value==null || value.Length==0 )
            {
                return BadRequest("No Tasks sent");
            }

            var result = this.service.GetProductById(id);

            if (result == null )
            {
                return NotFound("Product doesn't exists");
            }

            var tasks = this.taskService.GetTasks(value);

            if(tasks.Count() != value.Count())
            {
                if(tasks==null)
                {
                    return BadRequest("Tasks are not created");
                }

                var missingValues = value.Where(x => !tasks.Any(t => t.Id==x ));

                return BadRequest($"Tasks {missingValues.Select(x => x.ToString())} are not created");
            }

            result.TaskIds = result.TaskIds.Concat(value).ToList();

            this.service.UpdateProduct(result);

            return Ok();
        }

        [HttpPost("{id}/Materials")]
        public IActionResult MaterialPost(int id, [FromBody] int[] value)
        {
            if (value == null || value.Length == 0)
            {
                return BadRequest("No Materails sent");
            }

            var result = this.service.GetProductById(id);

            if (result == null)
            {
                return NotFound("Product doesn't exists");
            }

            var materials = this.materialService.GetByList(value);

            if (materials.Count() != value.Count())
            {
                if (materials == null)
                {
                    return BadRequest("Tasks are not created");
                }

                var missingValues = value
                        .Where(x => !materials.Any(t => t.Id == x));

                return BadRequest($"Tasks {missingValues.Select(x => x.ToString())} are not created");
            }

            if(result.Materials==null)
            {
                result.Materials = new List<int>();
            }

            result.Materials = result.Materials.Concat(value).ToList();

            this.service.UpdateProduct(result);

            return Ok();
        }

        [HttpPost]
        public IActionResult Post([FromBody]Product newProduct)
        {


            if (newProduct!= null && this.service.InsertProduct(newProduct))
            {
                return Ok();
            }

            return BadRequest();
        }

        
    }
}

