﻿using System.Collections.Generic;

namespace Repository
{
	public interface IElasticRepository<T> where T : class
	{
		bool Insert(T document);

        bool Update(T document);

		T FindById(int id);

		IEnumerable<T> GetAll();
	}
}
