﻿
namespace Repository.ElasticSearch
{
	using Nest;

	public interface IElasticSearchClient
	{
		IElasticClient Client { get; }
	}
}
