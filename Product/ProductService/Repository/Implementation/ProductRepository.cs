﻿namespace Repository.ElasticSearch
{
    using System.Collections.Generic;
    using System.Linq;
    using Model;
    using Nest;


    public class ProductRepository : IElasticRepository<Product>
	{
		private readonly IElasticSearchClient elasticClient;
		private readonly string Index;

		public ProductRepository(IElasticSearchClient client,string index)
		{
			this.elasticClient = client;
			this.Index = index;
		}

		public Product FindById(int id)
		{
			var result = this.elasticClient.Client.Search<Product>(
				x => x.Index(Index)
				.Query(q => q.ConstantScore(
					c => c.Filter(f => f.Term(t => t.Field(field => field.Id).Value(id))))));

			return result.Hits.Any()? result.Hits.FirstOrDefault().Source : null;
		}

		public IEnumerable<Product> GetAll()
		{
			var result = this.elasticClient.Client.Search<Product>(x => x.Index(Index));

			return result.Hits.Select(x => x.Source);
		}

		public bool Insert(Product document)
		{
			//Getting Id to the product
			document.Id = this.GetNewProductId();

			var result = this.elasticClient
			                 .Client
			                 .Index(document, x => x.Index(Index));
                
			return result.Created;
		}

        public bool Update(Product document)
        {
            

            var documentPath = new DocumentPath<Product>(document.Id).Index(Index);


            var result = this.elasticClient
                .Client.Update(documentPath,
                               x => x.Doc(document).Upsert(document).Index(Index).Fields(f => f));

            return true;
        }

        private int GetNewProductId()
		{
			var maxDocumentId = this.elasticClient
						.Client
						.Search<Product>(
							x => x.Index(Index).Sort(s => s.Descending(f => f.Id))).Hits.FirstOrDefault();

			if (maxDocumentId == null)
			{
				return 0;
			}

			return  maxDocumentId.Source.Id + 1;

		}
	}
}
