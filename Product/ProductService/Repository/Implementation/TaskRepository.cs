﻿

namespace Repository.ElasticSearch
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Model;

    public class TaskRepository : IElasticRepository<Task>
    {
		private readonly IElasticSearchClient elasticClient;
		private readonly string Index;

		public TaskRepository(IElasticSearchClient client, string index)
        {
            this.elasticClient = client;
            this.Index = index;
        }

        public Task FindById(int id)
        {
            var result = this.elasticClient.Client.Search<Task>(
			x => x.Index(Index)
			.Query(q => q.ConstantScore(
				c => c.Filter(f => f.Term(t => t.Field(field => field.Id).Value(id))))));

			return result.Hits.Any() ? result.Hits.FirstOrDefault().Source : null;
        }

        public IEnumerable<Task> GetAll()
        {
			var result = this.elasticClient.Client.Search<Task>(x => x.Index(Index));

			return result.Hits.Select(x => x.Source);
        }

        public bool Insert(Task document)
        {
			//Getting Id to the product
			document.Id = this.GetNewTaskId();

			var result = this.elasticClient
							 .Client
							 .Index(document, x => x.Index(Index));

			return true;
        }

        public bool Update(Task document)
        {
            var result = this.elasticClient
                              .Client
                              .Index(document, x => x.Index(Index));

            return true;
        }

        private int GetNewTaskId()
		{
			var maxDocumentId = this.elasticClient
						.Client
                                    .Search<Task>(
							x => x.Index(Index).Sort(s => s.Descending(f => f.Id))).Hits.FirstOrDefault();

			if (maxDocumentId == null)
			{
				return 0;
			}

			return maxDocumentId.Source.Id + 1;

		}
    }
}
