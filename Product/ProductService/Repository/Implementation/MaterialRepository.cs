﻿namespace Repository.ElasticSearch
{
    using System.Linq;
    using System.Collections.Generic;
    using ElasticSearch.Model;
    using Nest;

    public class MaterialRepository : IElasticRepository<Material>
    {
        private readonly IElasticSearchClient elasticClient;
        private readonly string Index;

        public  MaterialRepository(IElasticSearchClient elasticClient, string index)
        {
            this.elasticClient = elasticClient;
            this.Index = index;
        }

        public Material FindById(int id)
        {
            var result = this.elasticClient.Client.Search<Material>(
                x => x.Index(Index)
                .Query(q => q.ConstantScore(
                    c => c.Filter(f => f.Term(t => t.Field(field => field.Id).Value(id))))));

            return result.Hits.Any() ? result.Hits.FirstOrDefault().Source : null;
        }

        public IEnumerable<Material> GetAll()
        {
            var result = this.elasticClient.Client.Search<Material>(x => x.Index(Index));

            return result.Hits.Select(x => x.Source);
        }

        public bool Insert(Material document)
        {
            //Getting Id
            document.Id = this.GetNewMaterialId();

            var result = this.elasticClient
                             .Client
                             .Index(document, x => x.Index(Index));

            return result.Created;
        }

        public bool Update(Material document)
        {

            var documentPath = new DocumentPath<Material>(document.Id).Index(Index);


            var result = this.elasticClient
                .Client.Update(documentPath,
                               x => x.Doc(document).Upsert(document).Index(Index).Fields(f => f));

            return result.IsValid;
        }

        private int GetNewMaterialId()
        {
            var maxDocumentId = this.elasticClient
                        .Client
                                    .Search<Material>(
                            x => x.Index(Index).Sort(s => s.Descending(f => f.Id))).Hits.FirstOrDefault();

            if (maxDocumentId == null)
            {
                return 0;
            }

            return maxDocumentId.Source.Id + 1;

        }
    }
}
