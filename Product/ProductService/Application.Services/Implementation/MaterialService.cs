﻿namespace Application.Services
{
    using System;
    using System.Collections.Generic;
    using Application.DTO;
    using Repository;
    using System.Linq;
    using ElasticModel = Repository.ElasticSearch.Model;

    public class MaterialService : IMaterialService
    {
        private readonly IElasticRepository<ElasticModel.Material> materialRepository;

        public MaterialService(IElasticRepository<ElasticModel.Material> repository)
        {
            this.materialRepository = repository;
        }

        public IEnumerable<Material> GetAll()
        {
            return this.materialRepository.GetAll().Select(x => new Material
            {
                Code = x.Code,
                Description = x.Description,
                Id = x.Id
            }).ToList();
        }

        public Material GetById(int id)
        {
            var material =  this.materialRepository.FindById(id);

            if(material!=null)
            {
                return new Material
                {
                    Code = material.Code,
                    Description = material.Description,
                    Id = material.Id
                };
            }

            return null;
        }

        public IEnumerable<Material> GetByList(IEnumerable<int> ids)
        {
            var listOfTasks = new List<Material>();

            foreach (var id in ids)
            {
                var material = this.materialRepository.FindById(id);
                if(material!=null)
                {
                    listOfTasks.Add(new Material
                    {
                        Code = material.Code,
                        Description = material.Description,
                        Id = material.Id
                    });
                    
                }
            }

            return listOfTasks;
        }

        public bool Insert(Material material)
        {
            var convertedMaterial = new ElasticModel.Material
            {
                Code = material.Code,
                Description = material.Description,
                Id = material.Id

            };

            return this.materialRepository.Insert(convertedMaterial);
        }


    }
}
