﻿using System;
using System.Collections.Generic;
using Application.DTO;

namespace Application.Services
{
	public interface ITaskService
	{
		IEnumerable<Task> GetAllTasks();

        IEnumerable<Task> GetTasks(IEnumerable<int> taskId);

		Task GetTaskById(int id);

		bool InsertTask(Task product);

	}
}
