﻿using System;
using System.Collections.Generic;
using Application.DTO;

namespace Application.Services
{
    public interface IMaterialService
    {

        IEnumerable<Material> GetAll();

        IEnumerable<Material> GetByList(IEnumerable<int> ids);

        Material GetById(int id);

        bool Insert(Material material);
    }
}
