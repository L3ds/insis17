﻿

namespace Application.DTO
{
	using System.Collections.Generic;

	public class Product
	{

		public int Id
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public string Description
		{
			get;
			set;
		}

		public double Price
		{
			get;
			set;
		}

		public IEnumerable<int> TaskIds
		{
			get;
			set;
		}

        public IEnumerable<int> Materials
        {
            get;
            set;
        }
	}
}
