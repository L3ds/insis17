﻿using System;
namespace Application.DTO
{
    public class Material
    {
        public int Id
        {
            get;
            set;
        }

        public string Code
        {
            get;
            set;
   
        }

        public string Description
        {
            get;
            set;
        }

    }
}
