﻿
namespace Repository.ElasticSearch.Model
{
	using System.Collections.Generic;
	using Nest;

	public class Product
	{
		[Number(NumberType.Integer)]
		public int Id
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public string Description
		{
			get;
			set;
		}

		public double Price
		{
			get;
			set;
		}

		public IEnumerable<Task> Tasks
		{
			get;
			set;
		}

        public IEnumerable<Material> Material
        {
            get;
            set;
        }
    }
}
