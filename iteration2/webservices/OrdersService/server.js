// server.js
const MONGODB_PATH = 'mongodb://localhost:27017/OrderManagementDB';

// external packages
const express = require('express');
const app = express();
const bodyParser = require('body-parser');


// configure app to use bodyParse()
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// set port
const port = process.env.PORT || 3000;

// Connect to Database
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect(MONGODB_PATH);

// API Routes
const router = express.Router();
const orderRoutes = require('./api/routes/OrderRoutes');
const subscriptionRoutes = require('./api/routes/SubscriptionRoutes');

// middleware
router.use(function(req, res, next) {
	console.log(`Request Recieved: ${req.method} ${req.originalUrl}\nRequest Body:`);
	console.dir(req.body);
	console.log();
	next();
});

// Test route
router.get('/', function(req, res) {
	res.json({ message: 'Welcome to OrderManagement RESTful API!' });
});

// use Order Routes
orderRoutes(router);
// use Subscription Routes
subscriptionRoutes(router);

// Register Routes
app.use('/api', router);

// Start the Server
app.listen(port);

console.log(`OrderManagement RESTful API server started on: http://localhost:${port}`);
