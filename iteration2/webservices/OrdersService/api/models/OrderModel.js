// api/models/OrderModel.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
	productId: {
		type: String,
		required: true
	},

	quantity: {
		type: Number,
		default: 1
	},

	creationDate: {
		type: Date,
		default: Date.now
	},

	status: {
		type: Number,
		min: 0,
		max: 4,
		default: 0,
		required: true
	},

	timeEstimate: {
		type: Number,
		default: 0
	},

	costEstimate: {
		type: Number,
		default: 0
	},

	financialAvailability: {
		type: Number,
		min: 0,
		max: 3,
		default: 0
	},

	employeeId: {
		type: String,
		required: true
	}
});

OrderSchema.pre('save', function (next) {
 	if (!this.creationDate) {
  		this.creationDate = Date.now();
  	}

  	next();
});

module.exports = mongoose.model('Orders', OrderSchema);
